#!/bin/sh

#
# Copyright (C) 2015 shibby
#
# changes/fixes: 2018 - 2019 by pedro
#


PID=$$
PIDFILE="/var/run/watchdog.pid"
MWAN=$(nvram get mwan_num)
DST=$(nvram get mwan_ckdst)
HOSTLIST=$(echo $DST | sed 's/,/ /')
IPLIST=""
TMP_ROUTE_TABLE_ID=555

for HOST in $HOSTLIST; do
	IP=$(nslookup $HOST 2>/dev/null | grep "Address" | grep -v "::" | cut -d":" -f2 | cut -d' ' -f2 | head -n1)
	[ -z "$IP" ] && IP="8.8.8.8"
	IPLIST="$IPLIST $IP"
done

MWANTABLE="wan"
i=1
while [ $i -le $MWAN ]; do
	[ "$i" -gt 1 ] && MWANTABLE="$MWANTABLE wan$i"
	i=$((i+1))
done

LOGS="logger -t watchdog[$PID]"
[ "$(nvram get mwan_debug)" -gt 0 ] && {
	DEBUG="logger -p DEBUG -t watchdog[$PID]"
} || {
	DEBUG="echo"
}

watchdogRun() {
	for PREFIX in $MWANTABLE; do
		ISUP=$(wanuptime "$PREFIX")
		IFACE=$(nvram get "$PREFIX"_iface)
		ISPPPD=$([ -f /tmp/ppp/pppd$PREFIX ] && echo 1 || echo 0)
		WEIGHT=$(nvram get "$PREFIX"_weight)
		METHOD=$(nvram get "$PREFIX"_ckmtd)
		PROTO=$(nvram get "$PREFIX"_proto)
		DEMAND=$(nvram get "$PREFIX"_ppp_demand)
		RESULT=0
		PREFIX_MWAN=$PREFIX

		[ ! -z "$(nvram get "$PREFIX"_ck_pause)" ] && {
			$LOGS "Watchdog paused for $PREFIX - Skipping ..."
			continue
		}

		[ "$PROTO" != "disabled" ] && {
			ISGW=$(ip route | grep $IFACE | grep -v "link" | wc -l)
			$DEBUG "prefix=$PREFIX, iface=$IFACE, uptime=$ISUP, ISGW=$ISGW, WEIGHT=$WEIGHT"

			[ "$WEIGHT" -gt 0 -a "$ISUP" -gt 0 ] && {
				DEFAULT_ROUTE_FRAGMENT=$(ip route | grep default | cut -d' ' -f2-)
				GATEWAY_FRAGMENT="via $(nvram get "$PREFIX"_gateway)"
				[ "$ISPPPD" -eq 1 ] && GATEWAY_FRAGMENT=""
				for IP in $IPLIST; do
					[ ! -z "$DEFAULT_ROUTE_FRAGMENT" ] && {
						ROUTE_EXEC="ip route add $IP $DEFAULT_ROUTE_FRAGMENT"
						$DEBUG $ROUTE_EXEC
						$ROUTE_EXEC
					}
					ROUTE_EXEC="ip route add $IP dev $IFACE $GATEWAY_FRAGMENT metric 50000"
					$DEBUG $ROUTE_EXEC
					$ROUTE_EXEC
				done

				if [ "$METHOD" -eq 1 ]; then
					$DEBUG "run ping-test"
					ckping
				elif [ "$METHOD" -eq 2 ]; then
					$DEBUG "run tracert-test"
					cktracert
				else
					$DEBUG "run curl-test"
					ckcurl
				fi

				for IP in $IPLIST; do
					[ ! -z "$DEFAULT_ROUTE_FRAGMENT" ] && {
						ROUTE_EXEC="ip route del $IP $DEFAULT_ROUTE_FRAGMENT"
						$DEBUG $ROUTE_EXEC
						$ROUTE_EXEC
					}
					ROUTE_EXEC="ip route del $IP dev $IFACE $GATEWAY_FRAGMENT metric 50000"
					$DEBUG $ROUTE_EXEC
					$ROUTE_EXEC
				done
			}

			[ "$RESULT" -eq 0 -a "$WEIGHT" -eq 0 ] && {
				# failover does not have default gateway in route table, so we can't check connection to outside. Check only if interface exist
				$DEBUG "run failover-test"
				ckfailover
			}

			# wan is down
			[ "$RESULT" -eq 0 ] && {
				[ "$PROTO" == "lte" ] && {
					$LOGS "Connection $PREFIX DOWN - Reconnecting ..."
					echo "0" > "/var/lib/misc/"$PREFIX"_state"
					switch4g $PREFIX
				} || {
					[ "$PREFIX" == "wan" -a "$MWAN" -gt 1 ] && PREFIX_MWAN="wan1"

					[ "$(nvram get action_service)" == "wan-restart" -o "$(nvram get action_service)" == $PREFIX_MWAN"-restart" -o "$(nvram get action_service)" == "wan-restart-c" -o "$(nvram get action_service)" == $PREFIX_MWAN"-restart-c" ] && {
						$LOGS "Connection $PREFIX DOWN - Reconnect is already in progress ..."
					} || {
						echo "0" > "/var/lib/misc/"$PREFIX"_state"

						if [ "$PROTO" == "pppoe" -o "$PROTO" == "pptp" -o "$PROTO" == "l2tp" -o "$PROTO" == "ppp3g" ] && [ "$DEMAND" -eq 1 -a "$ISPPPD" -eq 0 ]; then
							$LOGS "Killing orphaned connect-on-demand listen process ..."
							LISTEN_PID=$(ps | grep listen | grep $PREFIX | awk '{print $1}' | head -n1)
							[ -n $LISTEN_PID ] && {
								kill -9 $LISTEN_PID
								$LOGS "Killed $LISTEN_PID"
							} || {
								$LOGS "Connect-on-demand listen not running"
							}

							$LOGS "Connection $PREFIX DOWN - Reconnecting ..."
							service $PREFIX_MWAN restart
						else
							$LOGS "Connection $PREFIX DOWN - Reconnect will be handled by another process ..."
						fi
					}
				}
			} || {
				$DEBUG "Connection $PREFIX is functioning"
				echo "1" > "/var/lib/misc/"$PREFIX"_state"
			}
		}
	done
}

cktracert() {
	RXBYTES1=$(cat /sys/class/net/$IFACE/statistics/rx_bytes)

	for IP in $IPLIST; do
		# we need only send/receive few packages to be sure is connection works.
		traceroute -i $IFACE -w 1 -m 4 -q 2 $IP > /dev/null 2>&1
	done

	RXBYTES2=$(cat /sys/class/net/$IFACE/statistics/rx_bytes)

	[ "$RXBYTES2" -gt "$RXBYTES1" ] && RESULT=1
	$DEBUG "tracert for $IFACE: RX2=$RXBYTES2 RX1=$RXBYTES1"
}

ckping() {
	local CHECK

	for IP in $IPLIST; do
		CHECK=$(ping -c 4 -I $IFACE $IP | grep "received" | cut -d "," -f2 | cut -d " " -f2)

		# "0" means 100% loss - not receive any package
		[ "$CHECK" -gt 0 ] && RESULT=$((RESULT+1))
		$DEBUG "$IFACE - $IP: $CHECK; ping OK=$RESULT"
	done

	[ "$RESULT" -gt 0 ] && $DEBUG "ping for $IFACE: OK=1"
}

ckcurl() {
	local CHECK

	for IP in $IPLIST; do
		CHECK=$(curl $IP --interface $IFACE --connect-timeout 5 -ksfI -o /dev/null && echo 1 || echo 0)

		[ "$CHECK" -gt 0 ] && RESULT=$((RESULT+1))
		$DEBUG "$IFACE - $IP: $CHECK; curl connect OK=$RESULT"
	done

	[ "$RESULT" -gt 0 ] && $DEBUG "curl connect for $IFACE: OK=1"
}

ckfailover() {
	[ "$(ip route | grep $IFACE | wc -l)" -gt 0 ] && {
		[ "$PROTO" == "lte" ] && {
			TYPE=$(nvram get "$PREFIX"_modem_type)
			DEV=$(nvram get "$PREFIX"_modem_dev)

			[ "$TYPE" == "non-hilink" -o "$TYPE" == "hw-ether" ] && {
				MODE="AT+CGPADDR=1" gcom -d "$DEV" -s /etc/gcom/setverbose.gcom > /tmp/4g_"$PREFIX".mode

				[ $(cat /tmp/4g_"$PREFIX".mode | grep "+CGPADDR:" | cut -d '"' -f2 | wc -l) -gt 0 ] && {
					RESULT=1
					$DEBUG "failover=1, lte $TYPE, dev $DEV"
				}
			} || {
				RESULT=1
				$DEBUG "failover=1, lte $TYPE"
			}
		} || {
			RESULT=1
			$DEBUG "failover=1"
		}
	}
}

watchdogAdd() {
	local CKTIME=$(nvram get mwan_cktime)
	local MINS=$((CKTIME/60))

	[ "$MINS" -gt 0 ] && {
		[ "$(cru l | grep watchdogJob | wc -l)" -eq 0 ] && cru a watchdogJob "*/$MINS * * * * /usr/sbin/watchdog"
	}
}

watchdogDel() {
	[ "$(cru l | grep watchdogJob | wc -l)" -eq 1 ] && cru d watchdogJob
}

mwanJob() {
	local ISSET=$(cru l | grep mwanJob | wc -l)

	[ "$MWAN" -gt 0 ] && {
		[ "$ISSET" -eq 0 ] && cru a mwanJob "*/1 * * * * /usr/sbin/watchdog alive"
	} || {
		[ "$ISSET" -eq 1 ] && cru d mwanJob
	}
}

mwanAlive() {
	[ "$MWAN" -gt 1 ] && {
		[ "$(ps | grep "mwanroute" | grep -v "grep" | wc -l)" -eq 0 ] && {
			$DEBUG "mwanroute not found, launch process"
			mwanroute
		}
	}
}

checkPid() {
	[ -f $PIDFILE ] && {
		PIDNO=$(cat $PIDFILE)
		cat "/proc/$PIDNO/cmdline" > /dev/null 2>&1

		[ $? -eq 0 ] && {
			$LOGS "Another process in action - Exiting ..."
			exit 0
		} || {
			# Process not found assume not running
			echo $PID > $PIDFILE
			[ $? -ne 0 ] && {
				$LOGS "Could not create PID file"
				exit 0
			}
		}
	} || {
		echo $PID > $PIDFILE
		[ $? -ne 0 ] && {
			$LOGS "Could not create PID file"
			exit 0
		}
	}
}

checkPidSwitch() {
	for PREFIX in $MWANTABLE; do
		[ -f /var/run/switch3g_$PREFIX.pid ] && {
			[ "$(ps | grep switch3g | grep -v "grep" | wc -l)" == "0" ] && {
				# pid file exists but process doesn't
				rm /var/run/switch3g_$PREFIX.pid
			} || {
				$LOGS "Switch3g ($PREFIX) script in action - Exiting ..."
				rm -f $PIDFILE > /dev/null 2>&1
				exit 0
			}
		}

		[ -f /var/run/switch4g_$PREFIX.pid ] && {
			[ "$(ps | grep switch4g | grep -v "grep" | wc -l)" == "0" ] && {
				# pid file exists but process doesn't
				rm /var/run/switch4g_$PREFIX.pid
			} || {
				$LOGS "Switch4g ($PREFIX) script in action - Exiting ..."
				rm -f $PIDFILE > /dev/null 2>&1
				exit 0
			}
		}
	done
}


###################################################


if [ "$1" == "add" ]; then
	watchdogAdd
	mwanJob
elif [ "$1" == "del" ]; then
	watchdogDel
elif [ "$1" == "alive" ]; then
	mwanAlive
else
	checkPid

	checkPidSwitch

	mwanJob

	watchdogRun
fi

rm -f $PIDFILE > /dev/null 2>&1
